﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PrimitiveDataTypes
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnTypeSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBoxItem selectedType = (type.SelectedItem as ListBoxItem);
            switch (selectedType.Content.ToString())
            {
                case "int":
                    ShowIntValue();
                    break;
                case "long":
                    ShowLongValue();
                    break;
                case "float":
                    ShowFloatValue();
                    break;
                case "double":
                    ShowDoubleValue();
                    break;
                case "decimal":
                    ShowDecimalValue();
                    break;
                case "string":
                    ShowStringValue();
                    break;
                case "char":
                    ShowCharValue();
                    break;
                case "bool":
                    ShowBoolValue();
                    break;
            }
        }

        private void ShowIntValue()
        {
            _txtSampleValue.Text = "to do";
        }

        private void ShowLongValue()
        {
            long longVar;
            longVar = 42L;
            _txtSampleValue.Text = longVar.ToString();
        }

        private void ShowFloatValue()
        {
            float floatVar;
            floatVar = 0.42F;
            _txtSampleValue.Text = floatVar.ToString();
        }

        private void ShowDoubleValue()
        {
            _txtSampleValue.Text = "to do";
        }

        private void ShowDecimalValue()
        {
            decimal decimalVar;
            decimalVar = 0.42M;
            _txtSampleValue.Text = decimalVar.ToString();
        }

        private void ShowStringValue()
        {
            string stringVar;
            stringVar = "forty two";
            _txtSampleValue.Text = stringVar; // ToString not needed
        }

        private void ShowCharValue()
        {
            char charVar;
            charVar = 'x';
            _txtSampleValue.Text = charVar.ToString();
        }

        private void ShowBoolValue()
        {
            _txtSampleValue.Text = "to do";
        }
    }
}
